//
//  valid_sudoku.swift
//  Problems
//
//  The Sudoku board could be partially filled, where empty cells are filled with the
//  character '.'.
//

import Foundation

func checkArray(array: Int?[]) -> Bool
{
    let validNumbers: Int[] = array.filter { $0 != nil }.map { $0! }
    let baseSet = NSSet(array: Array(1...9))
    let line = NSMutableSet(array: validNumbers)
    line.intersectSet(baseSet)

    return line.count == validNumbers.count
}

func validSudoku(board: String[]) -> Bool
{
    var handyBoard = board.map { Array($0).map { letter in "\(letter)".toInt() }  }

    // Lines
    for m in 0..9 {
        if !checkArray(handyBoard[m]) { return false }
    }

    // Columns
    for n in 0..9 {
        var column: Int?[] = []
        for m in 0..9 {
            column.append(handyBoard[m][n])
        }

        if !checkArray(column) { return false }
    }

    // Squares
    for i in 0..3 {
        for j in 0..3 {
            let square = handyBoard[(i * 3)][(j * 3)..(j * 3) + 3] +
                handyBoard[(i * 3) + 1][(j * 3)..(j * 3) + 3] +
                handyBoard[(i * 3) + 2][(j * 3)..(j * 3) + 3]

            if !checkArray(Array(square)) { return false }
        }
    }

    return true
}
