//
//  path_sum_II.swift
//  Problems
//
//  Given a binary tree and a sum, find all root-to-leaf paths where each path's sum equals
//  the given sum.

//  For example:
//  Given the below binary tree and sum = 22,
//
//        5
//       / \
//      4   8
//     /   / \
//    11  13  4
//   /  \    / \
//  7    2  5   1
//
//  return
//  [
//      [5,4,11,2],
//      [5,8,4,5]
//  ]
//

import Foundation

func pathSum(root:TreeNode?, aSum: Int, localPath: Int[], localSum: Int = 0) -> Int[][]
{
    if !root { return [] }

    let total = localSum + root!.val
    if !root!.left && !root!.right {
        return total == aSum ? [localPath + [root!.val]]: []
    }

    var paths: Int[][] = []
    if root!.left {
        paths += pathSum(root!.left, aSum, localPath + [root!.val], localSum: total)
    }

    if root!.right {
        paths += pathSum(root!.right, aSum, localPath + [root!.val], localSum: total)
    }

    return paths
}
