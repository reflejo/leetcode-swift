//
//  remove_nth_node_from_end_of_list.swift
//  Problems
//
//  Given a linked list, remove the nth node from the end of list and return its head.
//
//  For example,
//
//  Given linked list: 1->2->3->4->5, and n = 2.
//
//  After removing the second node from the end, the linked list becomes 1->2->3->5.
//
//  Note:
//  Given n will always be valid.
//  Try to do this in one pass.
//

import Foundation

func removeNthFromEnd(head: ListNode?, n: Int) -> ListNode?
{
    if !head { return nil }

    var newList:ListNode = ListNode(val: 0)
    newList.next = head

    var fast:ListNode? = newList
    var node:ListNode = newList

    for i in 0...n { fast = fast!.next }

    while fast {
        fast = fast!.next
        node = node.next!
    }

    node.next = node.next!.next

    return newList.next
}
