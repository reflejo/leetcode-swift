//
//  search_in_rotated_sorted_array.swift
//  Problems
//
//  Suppose a sorted array is rotated at some pivot unknown to you beforehand.
//
//  (i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).
//
//  You are given a target value to search. If found in the array return its index,
//  otherwise return -1.
//
//  You may assume no duplicate exists in the array.
//

import Foundation

func findPivot(A: Int[], start: Int = 0, anEnd: Int? = nil) -> Int
{
    let end = anEnd ? anEnd!: A.count - 1
    let mid = (start + end) / 2

    if mid - 1 > 0 && A[mid - 1] > A[mid] { return mid }
    if A[start] <= A[end] { return start }

    if A[start] > A[mid] {
        return findPivot(A, start: start, anEnd: mid - 1)
    }
    else {
        return findPivot(A, start: mid + 1, anEnd: end)
    }
}

func binarySearch(A: Int[], target: Int, pivot: Int, start: Int, end: Int) -> Int
{
    let mid = (start + end) / 2
    if start > end { return -1 }

    if A[mid % A.count] == target { return mid % A.count }

    if target > A[mid % A.count] {
        return binarySearch(A, target, pivot, mid + 1, end)
    }
    else {
        return binarySearch(A, target, pivot, start, end - 1)
    }
}

func search(A: Int[], target: Int) -> Int
{
    let pivot = findPivot(A)
    return binarySearch(A, target, pivot, pivot, A.count + pivot - 1)
}
