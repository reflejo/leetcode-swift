//
//  roman_to_integer.swift
//  Problems
//
//  Given a roman numeral, convert it to an integer.
//
//  Input is guaranteed to be within the range from 1 to 3999.
//
//

import Foundation

func romanToInt(s: String) -> Int
{
    let map = [
        "I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000
    ]
    var extended = s.uppercaseString
    extended = extended.stringByReplacingOccurrencesOfString("IV", withString: "IIII")
    extended = extended.stringByReplacingOccurrencesOfString("IX", withString: "VIIII")
    extended = extended.stringByReplacingOccurrencesOfString("XL", withString: "XXXX")
    extended = extended.stringByReplacingOccurrencesOfString("XC", withString: "LXXXX")
    extended = extended.stringByReplacingOccurrencesOfString("CD", withString: "CCCC")
    extended = extended.stringByReplacingOccurrencesOfString("CM", withString: "DCCCC")

    var result = 0
    for letter in extended {
        result += map["\(letter)"]!
    }

    return result
}
