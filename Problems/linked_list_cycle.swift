//
//  linked_list_cycle.swift
//  Problems
//
//  Given a linked list, determine if it has a cycle in it.
//

import Foundation

func hasCycle(list: ListNode?) -> Bool
{
    if !list { return false }

    var node = list
    var fast = list
    do {
        if !fast || !fast!.next { return false }

        fast = fast!.next!.next
        node = node!.next

    } while fast !== node

    return fast && node && fast === node
}
