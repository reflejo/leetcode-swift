//
//  maximum_depth_of_binary_tree.swift
//  Problems
//
//  Given a binary tree, find its maximum depth.
//
//  The maximum depth is the number of nodes along the longest path from the root node down to
//  the farthest leaf node.
//

import Foundation

func maximumDepth(root: TreeNode?) -> Int
{
    if !root { return 0 }

    var queue = [(1, root!)]
    var maxDepth = 0;
    while queue.count > 0 {
        let (depth, node) = queue.removeLast()
        if node.left {
            let left = (depth + 1, node.left!)
            queue.append(left)
        }

        if node.right {
            let right = (depth + 1, node.right!)
            queue.append(right)
        }
        maxDepth = max(maxDepth, depth)
    }

    return maxDepth
}
