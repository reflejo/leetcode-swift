//
//  3sum_closest.swift
//  Problems
//
//  Given an array S of n integers, find three integers in S such that the sum is closest
//  to a given number, target. Return the sum of the three integers. You may assume that each
//  input would have exactly one solution.
//
//  For example, given array S = {-1 2 1 -4}, and target = 1.
//
//  The sum that is closest to the target is 2. (-1 + 2 + 1 = 2).
//

import Foundation

func threeSumClosest(num: Int[], target: Int) -> Int {
    num.sort(>)

    var closest = Int(INT_MAX)
    var closestSum = 0
    for (i, n) in enumerate(num) {
        var hi = num.count - 1
        var lo = i + 1

        while lo < hi {
            let value = n + num[lo] + num[hi]
            if value == target { return value }

            if closest > abs(target - value) {
                closest = abs(target - value)
                closestSum = value
            }

            if value > target {
                hi--
            } else {
                lo++
            }
        }
    }

    return closestSum
}
