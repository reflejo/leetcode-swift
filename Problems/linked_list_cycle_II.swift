//
//  linked_list_cycle_II.swift
//  Problems
//
//  Given a linked list, return the node where the cycle begins. If there is no cycle,
//  return null.
//
//  Follow up:
//  Can you solve it without using extra space?
//

import Foundation

func detectCycle(head: ListNode?) -> ListNode?
{
    if !head { return nil }

    var node = head
    var fast = head
    do {
        fast = fast!.next!.next
        node = node!.next
    } while fast && fast!.next && node !== fast

    if !fast || !fast!.next { return nil }

    node = head
    while node !== fast {
        fast = fast!.next
        node = node!.next
    }

    return node
}
