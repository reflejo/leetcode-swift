//
//  flatten_binary_tree_to_linked_list.swift
//  Problems
//
//  Given a binary tree, flatten it to a linked list in-place.
//
//  For example,
//  Given
//
//      1
//     / \
//    2   5
//   / \   \
//  3   4   6
//
//  The flattened tree should look like:
//  1
//   \
//    2
//     \
//      3
//       \
//        4
//         \
//          5
//           \
//            6
//

import Foundation

func flatten(node: TreeNode?, head: ListNode? = nil) -> ListNode?  {
    if !node { return nil }

    var queue = [node!]
    let newList = ListNode(val: 0)
    var pointer = newList
    while queue.count > 0 {
        let node = queue.removeLast()

        pointer.next = ListNode(val: node.val)
        pointer = pointer.next!

        if node.right { queue.append(node.right!) }
        if node.left { queue.append(node.left!) }
    }

    return newList.next
}
