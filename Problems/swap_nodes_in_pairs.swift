//
//  swap_nodes_in_pairs.swift
//  Problems
//
//  Given a linked list, swap every two adjacent nodes and return its head.
//
//  For example,
//  Given 1->2->3->4, you should return the list as 2->1->4->3.
//
//  Your algorithm should use only constant space. You may not modify the values
//  in the list, only nodes itself can be changed.
//
//

import Foundation

func swapPairs(l1: ListNode?) -> ListNode?
{
    if !l1 { return nil }

    var head: ListNode?
    var node = l1
    var prev: ListNode?
    while node && node!.next {
        head = head ? head: node!.next

        let next = node!.next!
        let nextnext = node!.next!.next

        node!.next = node!.next!.next
        next.next = node

        if prev { prev!.next = next }

        prev = node
        node = nextnext
    }

    return head ? head: node
}
