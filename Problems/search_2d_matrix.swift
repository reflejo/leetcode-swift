//
//  search_2d_matrix.swift
//  Problems
//
//  Write an efficient algorithm that searches for a value in an m x n matrix. This matrix
//  has the following properties:
//
//  Integers in each row are sorted from left to right.
//  The first integer of each row is greater than the last integer of the previous row.
//  For example,
//
//  Consider the following matrix:
//

//  Top, Left = minimum
//  Bottom, Right = maximum
//  [
//      [1,   3,  5,  7],
//      [10, 11, 16, 20],
//      [23, 30, 34, 50]
//  ]
//
//  Given target = 3, return true.
//

import Foundation

func searchMatrix(matrix: Int[][], target: Int, aRow: Int? = nil, column: Int = 0) -> Bool
{
    let row = aRow ? aRow!: matrix.count - 1
    if row < 0 || column >= matrix[0].count { return false }

    if target == matrix[row][column] { return true }

    if target < matrix[row][column] {
        return searchMatrix(matrix, target, aRow: row - 1, column: column)
    }
    else {
        return searchMatrix(matrix, target, aRow: row, column: column + 1)
    }
}
