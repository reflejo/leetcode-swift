//
//  integer_to_roman.swift
//  Problems
//
//  Given an integer, convert it to a roman numeral.
//
//  Input is guaranteed to be within the range from 1 to 3999.
//

import Foundation

func intToRoman(num: Int) -> String {
    let map = [
        (1000, "M"), (500, "D"), (100, "C"),
        (50, "L"), (10, "X"), (5, "V"), (1, "I"),
    ]
    let reduceMap = [
        ("DCCCC", "CM"), ("CCCC", "CD"), ("LXXXX", "XC"),
        ("XXXX", "XL"), ("VIIII", "IX"), ("IIII", "IV"),
    ]

    var final = ""
    var number = num
    for (value, roman) in map {
        for i in 0..(number / value) {
            final += roman
            number -= value
        }
    }

    for (old, reduced) in reduceMap {
        final = final.stringByReplacingOccurrencesOfString(old, withString: reduced)
    }

    return final
}
