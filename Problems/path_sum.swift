//
//  path_sum.swift
//  Problems
//
//  Given a binary tree and a sum, determine if the tree has a root-to-leaf path such that
//  adding up all the values along the path equals the given sum.
//
//  For example:
//  Given the below binary tree and sum = 22,
//        5
//       / \
//      4   8
//     /   / \
//    11  13  4
//   /  \      \
//  7    2      1
//
//  return true, as there exist a root-to-leaf path 5->4->11->2 which sum is 22.
//

import Foundation

func hasPathSum(node: TreeNode?, val: Int, localSum: Int = 0) -> Bool
{
    if !node { return false }

    if (!node!.left && !node!.right) { return localSum + node!.val == val }

    return hasPathSum(node!.left, val, localSum: localSum + node!.val) ||
        hasPathSum(node!.right, val, localSum: localSum + node!.val)
}
