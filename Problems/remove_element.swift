//
//  remove_element.swift
//  Problems
//
//  Given an array and a value, remove all instances of that value in place and return the
//  new length.
//
//  The order of elements can be changed. It doesn't matter what you leave beyond the new length.
//

import Foundation

func removeElement(A: Int[], elm: Int) -> Int
{
    var lastI = -1
    for (i, n) in enumerate(A) {
        if n == elm && lastI < 0 {
            lastI = i
        }
        else if n != elm && lastI >= 0 {
            A[lastI] = n
            lastI += 1
        }
    }

    return lastI
}
