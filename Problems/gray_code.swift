//
//  gray_code.swift
//  Problems
//
//  The gray code is a binary numeral system where two successive values differ in only one bit.
//
//  Given a non-negative integer n representing the total number of bits in the code, print
//  the sequence of gray code. A gray code sequence must begin with 0.
//
//  For example, given n = 2, return [0,1,3,2]. Its gray code sequence is:
//
//  00 - 0
//  01 - 1
//  11 - 3
//  10 - 2
//  Note:
//  For a given n, a gray code sequence is not uniquely defined.
//
//  For example, [0,2,3,1] is also a valid gray code sequence according to the above definition.
//

import Foundation

func grayCode(n: Int) -> Int[] {
    if n == 1 { return [0, 1] }

    var options: Int[] = []
    var previousOptions = grayCode(n - 1)
    for b in previousOptions {
        options.append(b)
    }

    for b in reverse(previousOptions) {
        options.append((1 << (n - 1)) | b)
    }

    return options
}
