//
//  pascals_triangle.swift
//  Problems
//
//  Given numRows, generate the first numRows of Pascal's triangle.
//
//  For example, given numRows = 5,
//  Return
//
//  [
//          [1],
//         [1,1],
//        [1,2,1],
//       [1,3,3,1],
//      [1,4,6,4,1]
//  ]
//

import Foundation

func generate(numRows: Int) -> Int[][]
{
    if numRows == 0 { return [] }
    var result:Int[][] = Array(count: numRows, repeatedValue: [])

    result[0] = [1]
    for i in 1..numRows {
        for j in 0...i {
            let left = j - 1 >= 0 ? result[i - 1][j - 1]: 0
            let right = j < i ? result[i - 1][j]: 0
            result[i].append(left + right)
        }
    }

    return result
}
