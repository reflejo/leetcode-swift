//
//  balanced_binary_tree.swift
//  Problems
//
//  Given a binary tree, determine if it is height-balanced.
//
//  For this problem, a height-balanced binary tree is defined as a binary tree in which
//  the depth of the two subtrees of every node never differ by more than 1.
//

import Foundation

func isBalanced(node: TreeNode?) -> Bool {
    let (max_depth, is_balanced) = isBalanced(node, depth: 0)
    return is_balanced
}

func isBalanced(node: TreeNode?, depth: Int) -> (Int, Bool) {
    if !node { return (depth - 1, true) }

    let (leftDepth, leftBalanced) = isBalanced(node!.left, depth: depth + 1)
    let (rightDepth, rightBalanced) = isBalanced(node!.right, depth: depth + 1)

    let subtrees_distance = abs(leftDepth - rightDepth)
    return (max(leftDepth, rightDepth), subtrees_distance <= 1)
}
