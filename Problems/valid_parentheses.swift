//
//  valid_parentheses.swift
//  Problems
//
//  Given a string containing just the characters '(', ')', '{', '}', '[' and ']',
//  determine if the input string is valid.
//
//  The brackets must close in the correct order, "()" and "()[]{}" are all valid but "(]" and "([)]" are not.
//
//

import Foundation

func isValid(s: String) -> Bool
{
    var validCombinations = ["[": "]", "(": ")", "{": "}"]
    var stack: String[] = []

    for l in s {
        let letter = "\(l)"
        if validCombinations[letter] {
            stack.append(letter)
        }
        else {
            let open = stack.removeLast()
            if letter != validCombinations[open]! {
                return false
            }
        }
    }

    return stack.count == 0
}
