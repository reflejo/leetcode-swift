//
//  container_with_most_water.swift
//  Problems
//
//  Given n non-negative integers a1, a2, ..., an, where each represents a point at
//  coordinate (i, ai). n vertical lines are drawn such that the two endpoints of line i is at
//  (i, ai) and (i, 0). Find two lines, which together with x-axis forms a container, such that
//  the container contains the most water.
//
//  Note: You may not slant the container.
//
//

import Foundation

func maxArea(heights: Int[]) -> Int {
    var hi = heights.count - 1
    var low = 0
    var mArea = 0
    while low < hi {
        mArea = max(mArea, min(heights[low], heights[hi]) * (hi - low))
        if (heights[low] < heights[hi]) {
            low += 1
        } else {
            hi -= 1
        }
    }

    return mArea
}
