//
//  merge_two_sorted_list.swift
//  Problems
//
//  Merge two sorted linked lists and return it as a new list. The new list should be made
//  by splicing together the nodes of the first two lists.
//

import Foundation

func mergeTwoLists(l1: ListNode?, l2: ListNode?) -> ListNode?
{
    if !l1 { return l2 }
    if !l2 { return l1 }

    var newList: ListNode = ListNode(val: 0)
    let head: ListNode = newList

    var l1Pointer = l1, l2Pointer = l2
    while l1Pointer || l2Pointer {
        if !l2Pointer || (l1Pointer && l1Pointer!.val < l2Pointer!.val) {
            newList.next = l1Pointer
            newList = l1Pointer!

            l1Pointer = l1Pointer?.next
        }
        else {
            newList.next = l2Pointer
            newList = l2Pointer!

            l2Pointer = l2Pointer?.next
        }
    }

    return head.next
}
