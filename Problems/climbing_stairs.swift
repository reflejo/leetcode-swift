//
//  climbing_stairs.swift
//  Problems
//
//  You are climbing a stair case. It takes n steps to reach to the top. Each time you can
//  either climb 1 or 2 steps. In how many distinct ways can you climb to the top?
//

import Foundation

func climbStairs(n: Int) -> Int {
    var DP: Int[] = Array(count: n + 1, repeatedValue: 1)
    for var i = n - 1; i >= 0; --i {
        DP[i] = DP[i + 1] + (i + 2 < n + 1 ? DP[i + 2]: 0)
    }

    return DP[0]
}
