//
//  inorder_traversal.swift
//  Problems
//
//  Given a binary tree, return the inorder traversal of its nodes' values.
//

import Foundation

func inorderTraversal(root: TreeNode?) -> Int[] {
    if (!root) { return [] }

    var node = root
    var queue: TreeNode[] = []
    var result: Int[] = []
    while queue.count > 0 || node {
        if node {
            queue.append(node!)
            node = node!.left
        } else {
            node = queue.removeLast()
            result.append(node!.val)
            node = node!.right
        }
    }

    return result
}
