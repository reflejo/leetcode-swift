//
//  n_queens.swift
//  Problems
//
//  The n-queens puzzle is the problem of placing n queens on an n×n chessboard such
//  that no two queens attack each other.
//
//  Given an integer n, return all distinct solutions to the n-queens puzzle.
//
//  Each solution contains a distinct board configuration of the n-queens' placement,
//  where 'Q' and '.' both indicate a queen and an empty space respectively.
//
//  For example,
//  There exist two distinct solutions to the 4-queens puzzle:
//
//  [
//      [".Q..",  // Solution 1
//       "...Q",
//       "Q...",
//       "..Q."],
//
//      ["..Q.",  // Solution 2
//       "Q...",
//       "...Q",
//       ".Q.."]
//  ]
//

import Foundation


func isValid(m: Int, n: Int, queens: Int[]) -> Bool
{
    for (qm, qn) in enumerate(queens) {
        if m == qm || n == qn || abs(m - qm) == abs(n - qn) {
            return false
        }
    }

    return true
}

func solveNQueens(n: Int, m: Int = 0, queens: Int[] = []) -> Int
{
    if queens.count == n { return 1 }
    if m >= n { return 0 }

    var options = 0
    for i in 0..n {
        if isValid(m, i, queens) {
            options += solveNQueens(n, m: m + 1, queens: queens + [i])
        }
    }

    return options
}

//func solveNQueens(n: Int, m: Int = 0, queens: (Int, Int)[] = []) -> (Int, Int)[][]
//{
//    if queens.count == n { return [queens] }
//    if m >= n { return [] }
//
//    var options:(Int, Int)[][] = []
//    for i in 0..n
//    {
//        if isValid(m, i, queens)
//        {
//            let opts = solveNQueens(n, m: m + 1, queens: queens + [(m, i)])
//            options.extend(opts)
//        }
//    }
//
//    return options
//}
