//
//  unique_paths_II.swift
//  Problems
//
//  Follow up for "Unique Paths":
//
//  Now consider if some obstacles are added to the grids. How many unique paths would there be?
//
//  An obstacle and empty space is marked as 1 and 0 respectively in the grid.
//
//  For example,
//  There is one obstacle in the middle of a 3x3 grid as illustrated below.
//
//  [
//      [0,0,0],
//      [0,1,0],
//      [0,0,0]
//  ]
//  The total number of unique paths is 2.
//
//  Note: m and n will be at most 100.
//
//

import Foundation

func uniquePathsWithObstacles(obstacleGrid: Int[][]) -> Int
{
    if obstacleGrid.count == 0 { return 0 }
    var DP: Int[][] = []
    let M = obstacleGrid.count
    let N = obstacleGrid[0].count

    for i in 0..M {
        DP.append(Array(count: obstacleGrid[0].count, repeatedValue: 0))
    }

    DP[M - 1][N - 1] = 1

    for m in reverse(0..M) {
        for n in reverse(0..N) {
            if m + 1 == M && n + 1 == N { continue }

            let bottom:Int = m + 1 < M && obstacleGrid[m + 1][n] == 0 ? DP[m + 1][n]: 0
            let right:Int = n + 1 < N && obstacleGrid[m][n + 1] == 0 ? DP[m][n + 1]: 0

            DP[m][n] = bottom + right
        }
    }

    return DP[0][0]
}
