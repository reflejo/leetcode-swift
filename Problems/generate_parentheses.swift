//
//  generate_parentheses.swift
//  Problems
//
//  Given n pairs of parentheses, write a function to generate all combinations
//  of well-formed parentheses.
//
//  For example, given n = 3, a solution set is:
//
//  "((()))", "(()())", "(())()", "()(())", "()()()"
//

import Foundation

func generateParenthesis(n: Int, open: Int = 0, close: Int = 0, str: String = "") -> String[] {
    if close == n { return [str] }

    var options: String[] = []
    if (open < n) {
        options.extend(generateParenthesis(n, open: open + 1, close: close, str: str + "("))
    }

    if (open > close) {
        options.extend(generateParenthesis(n, open: open, close: close + 1, str: str + ")"))
    }

    return options
}
