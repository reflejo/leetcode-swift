//
//  same_tree.swift
//  Problems
//
//   Given two binary trees, write a function to check if they are equal or not.
//
//   Two binary trees are considered equal if they are structurally identical and the nodes have
//   the same value.
//

import Foundation

func sameTree(root1: TreeNode?, root2: TreeNode?) -> Bool
{
    if !root1 || !root2 { return !root1 && !root2 }

    if root1!.val != root2!.val { return false }

    return sameTree(root1!.left, root2!.left) &&
        sameTree(root1!.right, root2!.right)
}
