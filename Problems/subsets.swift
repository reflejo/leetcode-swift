//
//  subsets.swift
//  Problems
//
//  Given a set of distinct integers, S, return all possible subsets.
//

import Foundation

func subsets(S: Int[]) -> Int[][]
{
    var DP: Int[][][] = []
    for i in 0..S.count + 1 { DP.append([]) }

    for i in reverse(0..S.count) {
        DP[i] = DP[i + 1]
        DP[i].append([S[i]])
        for set in DP[i + 1] {
            DP[i].append([S[i]] + set)
        }
    }

    return DP[0]
}
