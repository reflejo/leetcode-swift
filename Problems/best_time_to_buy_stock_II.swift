//
//  best_time_to_buy_stock_II.swift
//  Problems
//
//  Say you have an array for which the ith element is the price of a given stock on day i.
//
//  Design an algorithm to find the maximum profit. You may complete as many transactions as
//  you like (ie, buy one and sell one share of the stock multiple times). However, you may not
//  engage in multiple transactions at the same time (ie, you must sell the stock before
//  you buy again).
//

import Foundation

func max_profit_stocks_II(prices: Int[]) -> Int {
    if prices.count == 0 { return 0 }

    var profit = 0
    for i in 1..prices.count {
        if prices[i] > prices[i - 1] {
            profit += prices[i] - prices[i - 1]
        }
    }

    return profit
}
