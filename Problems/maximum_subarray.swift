//
//  maximum_subarray.swift
//  Problems
//
//  Find the contiguous subarray within an array (containing at least one number) which has
//  the largest sum.
//

import Foundation

func maxSubArray(A: Int[]) -> Int
{
    var local_sum = 0, max_sum = 0, max_elm:Int = -1000000

    for elm in A {
        local_sum += elm
        if local_sum < 0 { local_sum = 0 }

        max_elm = max(max_elm, elm)
        max_sum = max(local_sum, max_sum)
    }

    return max(max_sum, max_elm)
}
