//
//  merge_sorted_array.swift
//  Problems
//
//  Given two sorted integer arrays A and B, merge B into A as one sorted array.
//
//  Note:
//  You may assume that A has enough space (size that is greater or equal to m + n) to hold
//  additional elements from B. The number of elements initialized in A and B are m
//  and n respectively.
//

import Foundation

func merge(A: Int[], m: Int, B: Int[])
{
    var Ai = m - 1
    var Bi = B.count - 1
    var i = A.count - 1
    while Ai >= 0 || Bi >= 0 {
        if Ai >= 0 && Bi >= 0 {
            A[i--] = A[Ai] > B[Bi] ? A[Ai--]: B[Bi--]
        }
        else {
            A[i--] = Ai >= 0 ? A[Ai--]: B[Bi--]
        }
    }
}
