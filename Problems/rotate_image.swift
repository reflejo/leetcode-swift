//
//  rotate_image.swift
//  Problems
//
//  You are given an n x n 2D matrix representing an image.
//
//  Rotate the image by 90 degrees (clockwise).
//
//  Follow up:
//  Could you do this in-place?
//

import Foundation

func rotate(matrix: Int[][]) -> Int[][]
{
    let M = matrix.count
    let N = M > 0 ? matrix[0].count: 0

    for p in 0..M / 2 {
        for k in p..N - p - 1 {
            var n = p
            var m = M - 1 - k
            var tmp = matrix[p][k]
            for i in 0..4 {
                (m, n) = (n, M - m - 1)
                (matrix[n][M - m - 1], tmp) = (tmp, matrix[n][M - m - 1])
            }

        }
    }

    return matrix
}
