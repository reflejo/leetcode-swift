//
//  best_time_to_buy_stock.swift
//  Problems
//
//  Say you have an array for which the ith element is the price of a given stock on day i.
//
//  If you were only permitted to complete at most one transaction (ie, buy one and sell one
//  share of the stock), design an algorithm to find the maximum profit.
//

import Foundation

func maxProfit(prices: Int[]) -> Int {
    var minPrice:Int = 10000
    var maxProfit = 0
    for price in prices {
        minPrice = min(price, minPrice)
        maxProfit = max(maxProfit, price - minPrice)
    }

    return maxProfit
}
