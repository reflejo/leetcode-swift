//
//  single_number.swift
//  Problems
//
//  Given an array of integers, every element appears twice except for one. Find that single one.
//
//  Note:
//  Your algorithm should have a linear runtime complexity. Could you implement it without
//  using extra memory?
//

import Foundation

func singleNumber(A: Int[]) -> Int
{
    return A.reduce(0) { $0 ^ $1 }
}
