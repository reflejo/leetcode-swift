//
//  single_number_II.swift
//  Problems
//
//  Created by Reflejo on 6/5/14.
//  Copyright (c) 2014 Fz. All rights reserved.
//

import Foundation

func singleNumberII(A: Int[]) -> Int
{
    var ones = 0, twos = 0, threes = 0;
    for i in 0..A.count {
        twos |= ones & A[i]
        ones ^= A[i]
        threes = ones & twos
        ones &= ~threes
        twos &= ~threes
    }

    return ones
}
