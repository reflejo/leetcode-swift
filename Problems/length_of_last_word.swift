//
//  length_of_last_word.swift
//  Problems
//
//  Given a string s consists of upper/lower-case alphabets and empty space characters ' ',
//  return the length of last word in the string.
//
//  If the last word does not exist, return 0.
//
//  Note: A word is defined as a character sequence consists of non-space characters only.
//
//  For example,
//
//  Given s = "Hello World",
//  return 5.
//

import Foundation

func lengthOfLastWord(s: String) -> Int
{
    var i:String.Index = s.startIndex
    var length = 0
    var spaceLast = false

    while s[i] != Character("\0") {
        if s[i] == Character(" ") {
            spaceLast = true
            while s[i] == Character(" ") { i = i.succ() }
        }

        if s[i] != Character("\0") {
            length = spaceLast ? 1: length + 1
            spaceLast = false
            i = i.succ()
        }
    }

    return length
}
