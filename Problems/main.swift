//
//  main.swift
//  Problems
//
//  Created by Reflejo on 6/3/14.
//  Copyright (c) 2014 Fz. All rights reserved.
//

import Foundation

assert(reverseWord("  the sky  is blue ") == ["blue", "is", "sky", "the"])

assert(evalRPN(["2", "1", "+", "3", "*"]) == 9)
assert(evalRPN(["4", "13", "5", "/", "+"]) == 6)

assert(
    maxPoints([Point(x: 0, y: 0), Point(x: 2, y: 2),
               Point(x: 3, y: 3), Point(x: 1, y: 1),
               Point(x: 2, y: 5), Point(x: 0, y: 0)]) == 5
)

assert(postorderTraversal(treeFromString("{4,2,5,1,3,#,6}")) == [1, 3, 2, 6, 5, 4])

assert(singleNumber([1, 2, 3, 4, 6, 1, 2, 3, 6]) == 4)

assert(maximumDepth(treeFromString("{1,#,2,#,3,#,4,#,5}")) == 5)
assert(maximumDepth(treeFromString("{4,2,5,1,3,#,6}")) == 3)

assert(sameTree(treeFromString("{4,2,5,1,3,#,6}"), treeFromString("{4,2,5,1,3,#,6}")))
assert(!sameTree(treeFromString("{4,5,2,1,3,#,6}"), treeFromString("{4,2,5,1,3,#,6}")))
assert(!sameTree(nil, treeFromString("{1}")))

assert(reverseDigits(1234) == 4321)
assert(reverseDigits(-1) == -1)
assert(reverseDigits(-12) == -21)

assert(max_profit_stocks_II([1, 10, 20, 2, 3, 5, 9]) == 26)
assert(max_profit_stocks_II([1, 0]) == 0)

assert(preorderTraversal(treeFromString("{1,#,2,3,#}")) == [1, 2, 3])
assert(preorderTraversal(treeFromString("{1,2,4,#,3,5,6}")) == [1, 2, 3, 4, 5, 6])


assert(!hasCycle(listFromString("1->2->3->4")))
assert(hasCycle(listFromString("1->2->3->4", loop: true)))

assert(inorderTraversal(treeFromString("{1,#,2,3,#}")) == [1, 3, 2])
assert(inorderTraversal(treeFromString("{4,2,6,1,3,5,7}")) == [1, 2, 3, 4, 5, 6, 7])

var tree = treeFromString("{1,2,3,4,5,6,7}")
connect(tree)
assert(tree.left?.next === tree.right)
assert(tree.left!.left!.next === tree.left!.right)
assert(tree.right!.next == nil)

assert(searchInsert([1,3,5,6], 5) == 2)
assert(searchInsert([1,3,5,6], 2) == 1)
assert(searchInsert([1,3,5,6], 7) == 4)
assert(searchInsert([1,3,5,6], 0) == 0)

assert(stringFromList(deleteDuplicates(listFromString("1->1->2->2->3->4"))) == "1->2->3->4")

assert(romanToInt("MCMXC") == 1990)
assert(romanToInt("MMXIV") == 2014)
assert(romanToInt("MCMIV") == 1904)

assert(climbStairs(5) == 8)
assert(climbStairs(10) == 89)
assert(climbStairs(20) == 10946)

assert(singleNumberII([1,1,1,-1,-2,-3,-1,-2,-3,-1,-2,-3,4]) == 4)

assert(maxSubArray([-2,1,-3,4,-1,2,1,-5,4]) == 6)

assert(intToRoman(1990) == "MCMXC")
assert(intToRoman(2014) == "MMXIV")
assert(intToRoman(1904) == "MCMIV")

var A = [3,3,4,5,8,9,3,4,5]
assert(removeElement(A, 3) == 6)

assert(
    stringFromList(mergeTwoLists(listFromString("1->2->4->8->20"), listFromString("1->4"))) == "1->1->2->4->4->8->20"
)

assert(!hasPathSum(treeFromString("{1,2,3,4,5,6,7}"), 20))
assert(hasPathSum(treeFromString("{1,2,3,4,5,6,7}"), 7))

assert(isBalanced(treeFromString("{1,2,3,4,5,6,7}")))
assert(!isBalanced(treeFromString("{1,2,3,#,#,6,7,8,#}")))

assert(stringFromList(swapPairs(listFromString("1->2->3->4"))) == "2->1->4->3")
assert(stringFromList(swapPairs(listFromString("1->2->3"))) == "2->1->3")


assert(sortedArrayToBST([1,2,3,4,5])!.val == 3)
assert(sortedArrayToBST([1,2,3,4,5,6,7])!.left!.val == 2)

assert(isSymmetric(treeFromString("{1,2,2,3,4,4,3}")))
assert(!isSymmetric(treeFromString("{1,2,2,#,3,#,3}")))

var B = [1,3,4,6,6,0,0]
merge(B, 5, [1,8])
assert(B == [1,1,3,4,6,6,8])

assert(solveNQueens(5) == 10)


