//
//  populate_right_pointer_II.swift
//  Problems
//
//  Follow up for problem "Populating Next Right Pointers in Each Node".
//
//  What if the given tree could be any binary tree? Would your previous solution still work?
//
//  Note:
//
//  You may only use constant extra space.
//  For example,
//  Given the following binary tree,
//
//      1
//     /  \
//    2    3
//   / \    \
//  4   5    7
//
//  After calling your function, the tree should look like:
//
//      1 -> NULL
//     /  \
//    2 -> 3 -> NULL
//   / \    \
//  4-> 5 -> 7 -> NULL

import Foundation


func connectII(root: TreeNode?)
{
    if !root { return }

    var queue = [(0, root!)]
    var lastDepth = -1
    var prev: TreeNode = root!
    while queue.count > 0 {
        let (depth, node) = queue.removeAtIndex(0)

        if lastDepth == depth {
            prev.next = node
            prev = node
        }
        else {
            prev = node
        }

        lastDepth = depth
        if node.left { queue.append((depth + 1, node.left!) as (Int, TreeNode)) }
        if node.right { queue.append((depth + 1, node.right!) as (Int, TreeNode)) }
    }
}
