//
//  minimum_depth_of_binary_tree.swift
//  Problems
//
//  Given a binary tree, find its minimum depth.
//
//  The minimum depth is the number of nodes along the shortest path from the root node down
//  to the nearest leaf node.
//

import Foundation

func minDepth(root: TreeNode?, depth: Int = 1, mDepth: Int = 10000) -> Int
{
    let isLeaf = !root!.left && !root!.right
    if !root || depth > mDepth || isLeaf { return min(depth, mDepth) }

    return min(
        root!.left ? minDepth(root!.left, depth: depth + 1, mDepth: mDepth): 100000,
        root!.right ? minDepth(root!.right, depth: depth + 1, mDepth: mDepth): 100000
    )
}
