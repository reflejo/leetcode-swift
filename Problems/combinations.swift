//
//  combinations.swift
//  Problems
//
//  Given two integers n and k, return all possible combinations of k numbers out of 1 ... n.
//
//  For example,
//  If n = 4 and k = 2, a solution is:
//
//  [
//      [2,4],
//      [3,4],
//      [2,3],
//      [1,2],
//      [1,3],
//      [1,4],
//  ]
//

import Foundation

func combine(n: Int, k: Int, start: Int = 1) -> Int[][] {
    if start > n || k > n || k == 0 { return [] }
    if k == 1 { return Array(start...n).map { [$0] } }
    if k == n { return [Array(start...n).map { $0 }] }

    var results: Int[][] = []
    for i in start...n {
        for comb in combine(n, k - 1, start: i + 1) {
            results.append([i] + comb)
        }
    }

    return results
}
