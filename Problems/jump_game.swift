//
//  jump_game.swift
//  Problems
//
//  Given an array of non-negative integers, you are initially positioned at the first index
//  of the array.
//
//  Each element in the array represents your maximum jump length at that position.
//
//  Determine if you are able to reach the last index.
//
//  For example:
//
//  A = [2,3,1,1,4], return true.
//  A = [3,2,1,0,4], return false.
//

import Foundation

func canJump(A: Int[]) -> Bool
{
    var DP: Bool[] = Array(count: A.count, repeatedValue: false)
    DP[A.count - 1] = true

    var minValid = A.count - 1
    for i in reverse(0..A.count - 1) {
        DP[i] = i + A[i] <= minValid
        if DP[i] { minValid = i }
    }

    return DP[0]
}
