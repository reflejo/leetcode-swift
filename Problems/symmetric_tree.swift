//
//  symmetric_tree.swift
//  Problems
//
//  Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
//

import Foundation

func sameTrees(n1: TreeNode?, n2:TreeNode?) -> Bool
{
    if !n1 || !n2 { return !n1 && !n2 }
    if n1!.val != n2!.val { return false }

    return sameTrees(n1!.left, n2!.right) || sameTrees(n1!.right, n2!.left)
}

func isSymmetric(node: TreeNode?) -> Bool
{
    if !node { return true }

    return sameTrees(node!.left, node!.right)
}
