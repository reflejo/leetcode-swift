//
//  convert_sorted_array_to_bst.swift
//  Problems
//
//  Given an array where elements are sorted in ascending order, convert it to a
//  height balanced BST.
//

import Foundation

func sortedArrayToBST(A: Int[], start: Int = 0, anEnd: Int? = nil) -> TreeNode?  {
    let end = anEnd ? anEnd!: A.count - 1

    if A.count == 0 { return nil }
    if start >= end { return TreeNode(val: A[start]) }

    let mid = (start + end) / 2

    let node = TreeNode(val: A[mid])
    node.left = sortedArrayToBST(A, start: start, anEnd: mid - 1)
    node.right = sortedArrayToBST(A, start: mid + 1, anEnd: end)

    return node
}
