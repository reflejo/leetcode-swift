//
//  postorder_traversal.swift
//  Problems
//
//  Given a binary tree, return the postorder traversal of its nodes' values.
//

import Foundation

func postorderTraversal(root: TreeNode?) -> Int[]
{
    if !root { return [] }

    var queue = [root!]
    var visited:Dictionary<Int, Bool> = Dictionary()
    var postorder: Int[] = []
    while queue.count > 0 {
        let node = queue[queue.count - 1]
        if !visited[node.val] {
            visited[node.val] = true

            if node.right { queue.append(node.right!) }
            if node.left { queue.append(node.left!) }
        }
        else {
            queue.removeLast()
            postorder.append(node.val)
        }
    }

    return postorder
}
