//
//  spiral_matrix_II.swift
//  Problems
//
//  Given an integer n, generate a square matrix filled with elements from 1 to n2 in
//  spiral order.
//
//  For example,
//  Given n = 3,
//
//  You should return the following matrix:
//  [
//      [ 1, 2, 3 ],
//      [ 8, 9, 4 ],
//      [ 7, 6, 5 ]
//  ]
//

import Foundation

enum Direction
{
    case Right, Bottom, Left, Top
}

let directionsMap:Dictionary<Direction, (Int, Int)> = [.Right: (0, 1), .Left: (0, -1),
    .Bottom: (1, 0), .Top: (-1, 0)]

func nextDirection(m: Int, n: Int, matrix: Int[][], direction: Direction) -> Direction
{
    let (dm, dn) = directionsMap[direction]!
    let mValid = m + dm < matrix.count && m + dm >= 0
    let nValid = n + dn < matrix[0].count && n + dn >= 0

    if !mValid || !nValid || matrix[m + dm][n + dn] > 0 {
        switch(direction) {
            case .Right:
                return .Bottom

            case .Bottom:
                return .Left

            case .Left:
                return .Top

            case .Top:
                return .Right
        }
    }

    return direction
}

func nextPosition(m: Int, n: Int, matrix: Int[][], inout direction: Direction) -> (Int, Int)
{
    direction = nextDirection(m, n, matrix, direction)
    let (dm, dn) = directionsMap[direction]!
    return (m + dm, n + dn)
}

func generateMatrix(num: Int) -> Int[][]
{
    var direction = Direction.Right
    var matrix: Int[][] = []
    for i in 0..num {
        matrix.append(Array(count: num, repeatedValue: 0))
    }

    var (m, n) = (0, 0)
    for i in 1...num * num {
        matrix[m][n] = i
        (m, n) = nextPosition(m, n, matrix, &direction)
    }

    return matrix
}
