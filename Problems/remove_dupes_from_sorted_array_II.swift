//
//  remove_dupes_from_sorted_array_II.swift
//  Problems
//
//  Follow up for "Remove Duplicates":
//  What if duplicates are allowed at most twice?
//
//  For example,
//  Given sorted array A = [1,1,1,2,2,3],
//
//  Your function should return length = 5, and A is now [1,1,2,2,3].

//

import Foundation

func removeDuplicatesII(A: Int[]) -> Int
{
    var lastI = 0
    var i = 0
    while i < A.count {
        let n = A[i]
        A[lastI++] = n

        if i + 1 < A.count && A[i + 1] == n { A[lastI++] = n }
        while i < A.count && A[i] == n { i++ }
    }

    return lastI
}
