//
//  binary_tree_level_order_traversal_II.swift
//  Problems
//
//  Given a binary tree, return the bottom-up level order traversal of its nodes' values.
//  (ie, from left to right, level by level from leaf to root).
//
//  For example:
//  Given binary tree {3,9,20,#,#,15,7},
//
//      3
//     / \
//    9  20
//       / \
//      15  7
//
//  return its bottom-up level order traversal as:
//  [
//      [15, 7],
//      [9, 20],
//      [3]
//  ]
//

import Foundation

func levelOrderBottom(root: TreeNode?) -> Int[][] {
    if !root { return [] }

    var result: Int[][] = []
    var queue = [(0, root!)]
    while queue.count > 0 {
        let (depth, node) = queue.removeLast()

        if depth >= result.count { result.append([]) }
        result[depth].append(node.val)

        if node.right { queue.append((depth + 1, node.right!) as (Int, TreeNode)) }
        if node.left { queue.append((depth + 1, node.left!) as (Int, TreeNode)) }
    }

    return result.reverse()
}
