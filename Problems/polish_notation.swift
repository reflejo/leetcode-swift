//
//  polish_notation.swift
//  Problems
//
//  Evaluate the value of an arithmetic expression in Reverse Polish Notation.
//
//  Valid operators are +, -, *, /. Each operand may be an integer or another expression.
//

import Foundation

func add(a: Int, b: Int) -> Int { return a + b }
func sub(a: Int, b: Int) -> Int { return a - b }
func mul(a: Int, b: Int) -> Int { return a * b }
func div(a: Int, b: Int) -> Int { return a / b }

func evalRPN(tokens: String[]) -> Int
{
    var mutableTokens = tokens
    let operators = [
        "+": add,
        "-": sub,
        "/": div,
        "*": mul
    ]

    var i = 0
    while mutableTokens.count > 1 {
        var term = mutableTokens[i++]
        if let operator = operators[term] {
            var result = operator(mutableTokens[i - 3].toInt()!,
                                  mutableTokens[i - 2].toInt()!)
            mutableTokens[i - 1] = "\(result)"
            mutableTokens.removeAtIndex(i - 3)
            mutableTokens.removeAtIndex(i - 3)
            i -= 2
        }
    }

    return mutableTokens[0].toInt()!
}
