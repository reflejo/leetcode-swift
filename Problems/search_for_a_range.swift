//
//  search_for_a_range.swift
//  Problems
//
//  Given a sorted array of integers, find the starting and ending position of a given
//  target value.
//
//  Your algorithm's runtime complexity must be in the order of O(log n).
//
//  If the target is not found in the array, return [-1, -1].
//
//  For example,
//  Given [5, 7, 7, 8, 8, 10] and target value 8,
//  return [3, 4].
//
//

import Foundation

func searchRange(A: Int[], target: Int, start: Int = 0, end: Int? = nil) -> (Int, Int)
{
    let _end = end ? end!: A.count - 1
    let mid = (start + _end) / 2

    if start > _end { return (-1, -1) }

    if A[mid] == target {
        var lo = mid
        var hi = mid
        while lo >= 0 && A[lo] == target { lo-- }
        while hi < A.count && A[hi] == target { hi++ }

        return (lo + 1, hi - 1)
    }

    if target > A[mid] {
        return searchRange(A, target, start: mid + 1, end: end)
    }
    else {
        return searchRange(A, target, start: start, end: mid - 1)
    }
}
