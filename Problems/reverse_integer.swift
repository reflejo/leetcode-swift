//
//  reverse_integer.swift
//  Problems
//
//  Reverse digits of an integer.
//

import Foundation

func reverseDigits(x: Int) -> Int
{
    let is_negative = x < 0
    var n = abs(x)
    var result = 0

    while n > 0 {
        result = (result * 10) + (n % 10)
        n /= 10
    }

    return is_negative ? -result: result
}
