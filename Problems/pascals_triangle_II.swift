//
//  pascals_triangle_II.swift
//  Problems
//
//  Given an index k, return the kth row of the Pascal's triangle.
//
//  For example, given k = 3,
//  Return [1,3,3,1].
//
//  Note:
//  Could you optimize your algorithm to use only O(k) extra space?
//
//

import Foundation

func getRow(rowIndex: Int) -> Int[]
{
    var result: Int[] = Array(count: rowIndex + 1, repeatedValue: 0)

    result[0] = 1
    var previous = 1
    for i in 1...rowIndex {
        for j in 0...i {
            let left = j - 1 >= 0 ? previous: 0
            let right = j < i ? result[j]: 0
            previous = result[j]
            result[j] = right + left
        }
    }

    return result
}
