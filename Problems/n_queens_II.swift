//
//  n_queens_II.swift
//  Problems
//
//  Follow up for N-Queens problem.
//
//  Now, instead outputting board configurations, return the total number of distinct solutions.
//
//

import Foundation

func _solveNQueensII(n: Int, m: Int = 0, queens: Int[] = []) -> Int[][]
{
    if queens.count == n { return [queens] }
    if m >= n { return [] }

    var options:Int[][] = []
    for i in 0..n {
        if isValid(m, i, queens) {
            let opts = _solveNQueensII(n, m: m + 1, queens: queens + [i])
            options.extend(opts)
        }
    }

    return options
}

func solveNQueensII(n: Int, m: Int = 0, queens: (Int, Int)[] = []) -> String[][]
{
    var solutions:String[][] = []
    for solution in _solveNQueensII(n) {
        var board: String[] = []
        for qn in solution {
            let line = Array(count: n, repeatedValue: ".")
            line[qn] = "Q"
            board.append("".join(line))
        }
        solutions.append(board)
    }

    return solutions
}
