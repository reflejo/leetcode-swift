//
//  set_matrix_zeroes.swift
//  Problems
//
// Given a m x n matrix, if an element is 0, set its entire row and column to 0. Do it in place.
//

import Foundation

func setZeroes(matrix: Int[][])
{
    if matrix.count == 0 { return }

    let M = matrix.count
    let N = matrix[0].count

    for m in 0..M {
        for n in 0..N {
            if matrix[m][n] == 0 {
                for i in 0..N { matrix[m][i] = matrix[m][i] == 0 ? 0: -1 }
                for i in 0..M { matrix[i][n] = matrix[i][n] == 0 ? 0: -1 }
            }
        }
    }

    for m in 0..M {
        for n in 0..N {
            matrix[m][n] = matrix[m][n] == -1 ? 0: matrix[m][n]
        }
    }
}
