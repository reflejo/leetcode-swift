//
//  palindrome_number.swift
//  Problems
//
//  Determine whether an integer is a palindrome. Do this without extra space.
//

import Foundation

func isPalindrome(x: Int) -> Bool
{
    var n:Int = x
    var digits:Int = Int(log10(Double(x))) + 1

    while digits > 1 {
        let n1 = n % 10
        let n2 = n / Int(pow(10.0, Double(digits - 1)))

        if n1 != n2 { return false }

        n = n % Int(pow(10.0, Double(digits - 1)))
        n /= 10
        digits -= 2
    }

    return true
}
