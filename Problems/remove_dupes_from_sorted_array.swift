//
//  remove_dupes_from_sorted_array.swift
//  Problems
//
//  Given a sorted array, remove the duplicates in place such that each element appear only
//  once and return the new length.
//
//  Do not allocate extra space for another array, you must do this in place with constant memory.
//
//  For example,
//  Given input array A = [1,1,2],
//
//  Your function should return length = 2, and A is now [1,2].
//

import Foundation

func removeDuplicates(A: Int[]) -> Int
{
    var lastI = 0
    var i = 0
    while i < A.count {
        let n = A[i]

        A[lastI++] = n
        while i < A.count && A[i] == n { i++ }
    }

    return lastI
}
