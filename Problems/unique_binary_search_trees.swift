//
//  unique_binary_search_trees.swift
//  Problems
//
//  Given n, how many structurally unique BST's (binary search trees) that store values 1...n?
//
//  For example,
//  Given n = 3, there are a total of 5 unique BST's.
//
//  1         3     3      2      1
//   \       /     /      / \      \
//    3     2     1      1   3      2
//   /     /       \                 \
//  2     1         2                 3
//

import Foundation

var cache: Dictionary<String, Int> = Dictionary()

func numTrees(end: Int, start: Int = 1) -> Int
{
    var key:String = "\(end,start)"
    if let cached = cache[key] { return cached }
    if end <= start { return 1 }

    var result:Int = 0
    for i in start...end {
        result += numTrees(i - 1, start: start) * numTrees(end, start: i + 1)
    }

    cache[key] = result
    return result
}
