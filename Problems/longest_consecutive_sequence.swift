//
//  longest_consecutive_sequence.swift
//  Problems
//
//  Given an unsorted array of integers, find the length of the longest consecutive
//  elements sequence.
//
//  For example,
//  Given [100, 4, 200, 1, 3, 2],
//  The longest consecutive elements sequence is [1, 2, 3, 4]. Return its length: 4.
//
//  Your algorithm should run in O(n) complexity.
//
//

import Foundation

func longestConsecutive(num: Int[]) -> Int
{
    var hashMap: Dictionary<Int, (Int, Int)> = Dictionary()
    var biggest = 0

    for n in num {
        var _min = hashMap[n - 1] ? hashMap[n - 1]!.0: n
        var _max = hashMap[n + 1] ? hashMap[n + 1]!.1: n
        if _min != n {
            hashMap[n - 1] = (_min, _max)
        }

        if _max != n {
            hashMap[n + 1] = (_min, _max)
        }

        hashMap[n] = (_min, _max)
        biggest = max(biggest, _max - _min)
    }

    return biggest + 1
}
