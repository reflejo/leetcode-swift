//
//  minimum_path_sum.swift
//  Problems
//
//  Given a m x n grid filled with non-negative numbers, find a path from top left to bottom
//  right which minimizes the sum of all numbers along its path.
//
//  Note: You can only move either down or right at any point in time.
//
//

import Foundation

func minPathSum(grid: Int[][]) -> Int
{
    let M = grid.count
    let N = M > 0 ? grid[0].count: 0

    var DP: Int[][] = []
    for m in 0..M {
        DP.append(Array(count: N, repeatedValue: 0))
    }

    for m in reverse(0..M) {
        for n in reverse(0..N) {
            let bottom:Int = m + 1 < M ? DP[m + 1][n]: 100000
            let right:Int = n + 1 < N ? DP[m][n + 1]: 100000

            var minimum:Int = min(bottom, right)
            if n + 1 >= N && m + 1 >= M { minimum = 0 }

            DP[m][n] = minimum + grid[m][n]
        }
    }

    return DP[0][0]
}
