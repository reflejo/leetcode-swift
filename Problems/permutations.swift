//
//  permutations.swift
//  Problems
//
//  Given a collection of numbers, return all possible permutations.
//

import Foundation

func permute(num: Int[]) -> Int[][]
{
    if num.count == 0 { return [] }
    if num.count == 1 { return [ [num[0]] ] }

    var result: Int[][] = []
    for i in 0..num.count {
        let next: Int[] = Array(num[0..i] + num[i+1..num.count])
        for perm in permute(next) {
            result.append([num[i]] + perm)
        }
    }

    return result
}
