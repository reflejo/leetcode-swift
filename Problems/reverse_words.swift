//
//  reverse_words.swift
//  Problems
//
//  Given an input string, reverse the string word by word.
//
//  For example,
//  Given s = "the sky is blue",
//  return "blue is sky the".

import Foundation

func reverseWord(s: String) -> String[]
{
    var words: String[] = []
    var word = String()

    for letter in reverse(s) {
        if letter == " " {
            if !word.isEmpty { words.append(word) }
            word = ""
        }
        else {
            word = letter + word
        }
    }

    if !word.isEmpty { words.append(word) }
    return words
}
