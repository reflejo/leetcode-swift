//
//  longest_common_prefix.swift
//  Problems
//
//  Write a function to find the longest common prefix string amongst an array of strings.
//

import Foundation

func listFromWord(word: String) -> ListNode
{
    var head: ListNode = ListNode(val: 0)
    var p: ListNode = head

    for i in 0..word.utf16count {
        p.next = ListNode(val: Int(word.utf16[i]))
        p = p.next!
    }

    return head.next!
}

func countFromList(head: ListNode, word: String) -> Int
{
    var node: ListNode? = head
    for i in 0..word.utf16count {
        if !node || node!.val != Int(word.utf16[i]) { return i }
        node = node!.next
    }

    return word.utf16count
}

func longestCommonPrefix(strs: String[]) -> String
{
    var list = listFromWord(strs[0])
    var minPrefix = 1000
    for word in strs {
        minPrefix = min(minPrefix, countFromList(list, word))
    }

    return strs[0].substringToIndex(minPrefix)
}
