//
//  plus_one.swift
//  Problems
//
//  Given a non-negative number represented as an array of digits, plus one to the number.
//
//  The digits are stored such that the most significant digit is at the head of the list.
//
//

import Foundation

func plusOne(digits: Int[]) -> Int[]
{
    var result: Int[] = []
    var carry = 1
    for digit in reverse(digits) {
        result.append((digit + carry) % 10)
        carry = (digit + carry) / 10
    }

    if carry > 0 { result.append(carry) }

    return result.reverse()
}
