//
//  preorder_traversal.swift
//  Problems
//
//  Given a binary tree, return the preorder traversal of its nodes' values
//

import Foundation

func preorderTraversal(root: TreeNode?) -> Int[]
{
    if !root { return [] }

    var result: Int[] = []
    var queue = [root!]
    while queue.count > 0 {
        let node = queue.removeLast()

        result.append(node.val)
        if (node.right) { queue.append(node.right!) }
        if (node.left) { queue.append(node.left!) }
    }

    return result
}
