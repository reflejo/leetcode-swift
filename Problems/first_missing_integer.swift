//
//  first_missing_integer.swift
//  Problems
//
//  Created by Martin Conte Mac Donell on 6/5/14.
//  Copyright (c) 2014 Fz. All rights reserved.
//

import Foundation

func firstMissingPositive(inout A: Int[]) -> Int {
    for num in A {
        var next = num
        while next < A.count && next > 0 {
            next = A[num - 1]
            A[num - 1] = num
        }
    }

    for (i, num) in enumerate(A) {
        if i != num { return i }
    }

    return A.count + 1
}
