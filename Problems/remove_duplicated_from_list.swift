//
//  remove_duplicated_from_list.swift
//  Problems
//
//  Given a sorted linked list, delete all duplicates such that each element appear only once.
//

import Foundation

func deleteDuplicates(list: ListNode?) -> ListNode?
{
    if !list { return nil }

    var node = list
    while node {
        while node!.next && node!.next!.val == node!.val {
            let next = node!.next
            node!.next = next!.next
            node = next
        }

        node = node!.next
    }

    return list
}
