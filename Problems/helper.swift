//
//  helper.swift
//  Problems
//
//  Created by Reflejo on 6/4/14.
//  Copyright (c) 2014 Fz. All rights reserved.
//

import Foundation

func factorial(n: Int) -> Int {
    var result = n
    for i in 1..n { result *= i }
    return result
}

class TrieNode {
    var val: String
    var adjacent: Dictionary<String, TrieNode>

    init(val: String) {
        self.val = val
        self.adjacent = Dictionary()
    }
}

//
// Trees Helpers
//

class TreeNode {
    var val: Int
    var left: TreeNode?, right: TreeNode?, next: TreeNode?

    init(val: Int) {
        self.val = val
    }
}

func treeFromString(s: String) -> TreeNode {
    var s2 = s.substringFromIndex(1).substringToIndex(s.utf16count - 2)
    var nodes = s2.componentsSeparatedByString(",")

    var head = TreeNode(val: nodes[0].toInt()!)
    var treeNodes: TreeNode?[] = [head]
    var i = 1
    while i < nodes.count {
        let root = treeNodes.removeAtIndex(0)
        if nodes[i] != "#" {
            root!.left = TreeNode(val: nodes[i].toInt()!)
            treeNodes.append(root!.left)
        }

        if nodes[i + 1] != "#" {
            root!.right = TreeNode(val: nodes[i + 1].toInt()!)
            treeNodes.append(root!.right)
        }

        i += 2
    }

    return head
}

//
// Linked Lists Helpers
//

class ListNode {
    var val: Int
    var next: ListNode?

    init(val: Int) {
        self.val = val
    }
}

func stringFromList(head: ListNode?) -> String {
    var node = head
    var values: String[] = []
    while node {
        values.append("\(node!.val)")
        node = node!.next
    }

    return "->".join(values)
}

func listFromString(s: String, loop: Bool = false) -> ListNode?  {
    var head = ListNode(val: 0)
    var pointer = head
    for val in s.componentsSeparatedByString("->") {
        pointer.next = ListNode(val: val.toInt()!)
        pointer = pointer.next!
    }

    if loop { pointer.next = head.next }

    return head.next
}
