//
//  trapping_rain_water.swift
//  Problems
//
//  Given n non-negative integers representing an elevation map where the width of each
//  bar is 1, compute how much water it is able to trap after raining.
//
//  For example,
//  Given [0,1,0,2,1,0,1,3,2,1,2,1], return 6.
//
//

import Foundation

func trap(A: Int[]) -> Int
{
    var hi = A.count - 1
    var lo = 0

    while lo + 1 < A.count && A[lo + 1] > A[lo] { lo++ }
    while hi - 1 >= 0 && A[hi - 1] > A[hi] { hi-- }

    var result = 0
    var hiValue = 0
    var loValue = 0
    while lo < hi {
        loValue = max(A[lo], loValue)
        hiValue = max(A[hi], hiValue)
        if A[lo] < A[hi] {
            lo++
            if lo < A.count && A[lo] < loValue { result += loValue - A[lo] }
        }
        else {
            hi--
            if hi >= 0 && A[hi] < hiValue { result += hiValue - A[hi] }
        }
    }

    return result
}
