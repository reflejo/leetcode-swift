//
//  max_points.swift
//  Problems
//
//  Given n points on a 2D plane, find the maximum number of points that lie
//  on the same straight line.
//

import Foundation

class Point
{
    var x:Int, y:Int

    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
}


@infix func == (left: Point, right: Point) -> Bool
{
    return left.x == right.x && left.y == right.y
}

func gdc(numerator: Int, denominator: Int) -> String
{
    var (a, b) = (numerator, denominator)
    while b > 0 {
        let tmp = a
        a = b
        b = tmp % b
    }

    if a == 0 {
        return "0, 0"
    }
    else {
        return "\(numerator / a), \(denominator / a)"
    }
}

func maxPoints(points: Point[]) -> Int
{
    var max_points:Int = 0

    for (i, point1) in enumerate(points) {
        var slopes:Dictionary<String, Int> = Dictionary()
        var same:Int = 1
        var local_max:Int = 0
        for j in i + 1..points.count {
            let point2 = points[j]
            let slope = gdc(point2.y - point1.y, point2.x - point1.x)

            if point1 == point2 {
                same += 1
                continue
            }

            if let value = slopes[slope] {
                slopes[slope] = value + 1
            }
            else
            {
                slopes[slope] = 1
            }

            local_max = max(slopes[slope]!, local_max)
        }

        max_points = max(local_max + same, max_points)
    }

    return max_points
}
