//
//  search_insert_position.swift
//  Problems
//
//  Given a sorted array and a target value, return the index if the target is found. If not,
//  return the index where it would be if it were inserted in order.
//
//  You may assume no duplicates in the array.
//

import Foundation

func searchInsert(A: Int[], target: Int, start: Int = 0, anEnd: Int? = nil) -> Int
{
    var end = anEnd != nil ? anEnd!: A.count - 1
    let mid = (start + end) / 2

    if start > end  { return start }

    if target == A[mid] {
        return mid
    }
    else if target > A[mid] {
        return searchInsert(A, target, start: mid + 1, anEnd: end)
    }
    else {
        return searchInsert(A, target, start: start, anEnd: mid - 1)
    }
}
