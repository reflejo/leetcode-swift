//
//  sort_colors.swift
//  Problems
//
//  Given an array with n objects colored red, white or blue, sort them so that objects of
//  the same color are adjacent, with the colors in the order red, white and blue.
//
//  Here, we will use the integers 0, 1, and 2 to represent the color red, white,
//  and blue respectively.
//

import Foundation

func sortColors(A: Int[])
{
    var zerosP = -1
    var onesP =  -1
    var twosP = -1

    for (i, n) in enumerate(A) {
        A[i] = 2
        if n == 0 {
            A[++onesP] = 1
            A[++zerosP] = 0
        }
        else if n == 1 {
            A[++onesP] = 1
        }
    }
}
