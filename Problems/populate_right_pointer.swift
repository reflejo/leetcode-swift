//
//  populate_right_pointer.swift
//  Problems
//
//  Given a binary tree
//
//  struct TreeLinkNode {
//      TreeLinkNode *left;
//      TreeLinkNode *right;
//      TreeLinkNode *next;
//  }
//  Populate each next pointer to point to its next right node. If there is no next right node,
//  the next pointer should be set to NULL.
//

import Foundation

func connect(root: TreeNode?)
{
    if !root { return }

    var queue = [(0, root!)]
    var levels: TreeNode[] = []
    while queue.count > 0 {
        let (depth, node) = queue.removeLast()

        if (depth >= levels.count) {
            levels.append(node)
        }
        else {
            levels[depth].next = node
            levels[depth] = node
        }

        if (node.right) {
            let right = (depth + 1, node.right!)
            queue.append(right)
        }

        if (node.left) {
            let left = (depth + 1, node.left!)
            queue.append(left)
        }
    }
}
